from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField, IntegerField, TextAreaField, SelectField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError, Optional
from flaskblog.models import Users, Movies


class RegistrationForm(FlaskForm):
    username = StringField('Username',
                           validators=[DataRequired(), Length(min=2, max=20)])
    email = StringField('Email',
                        validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password',
                                     validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Sign Up')

    def validate_username(self, username):
        user = Users.query.filter_by(username=username.data).first()
        if user:
            raise ValidationError('That username is taken. Please choose a different one.')

    def validate_email(self, email):
        user = Users.query.filter_by(email=email.data).first()
        if user:
            raise ValidationError('That email is taken. Please choose a different one.')


class LoginForm(FlaskForm):
    email = StringField('Email',
                        validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember = BooleanField('Remember Me')
    submit = SubmitField('Login')


class AdminMovieForm(FlaskForm):
    tconst = StringField('Tconst', validators=[DataRequired()])
    type = StringField('Type', validators=[DataRequired()])
    title = StringField('Title', validators=[DataRequired()])
    start_year = IntegerField('Start Year', validators=[DataRequired()])
    end_year = IntegerField('End Year', validators=[Optional()])
    runtime = IntegerField("Runtime", validators=[Optional()])
    picture = StringField("Picture", validators=[Optional()])

    def validate_tconst(self, tconst):
        movie = Movies.query.filter_by(tconst=tconst.data).first()
        if movie:
            raise ValidationError('Movie is already in database')


class AdminHumanForm(FlaskForm):
    nconst = StringField('Nconst', validators=[DataRequired()])
    name = StringField('Name', validators=[DataRequired()])
    birth_year = IntegerField('Birth Year', validators=[Optional()])
    death_year = IntegerField('Death Year', validators=[Optional()])
    picture = StringField("Picture", validators=[Optional()])

    def validate_tconst(self, nconst):
        human = Humans.query.filter_by(nconst=nconst.data).first()
        if movie:
            raise ValidationError('Human is already in database')


class CreateMovieListForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    description = TextAreaField('Description', validators=[DataRequired()])


class CreateHumanListForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    description = TextAreaField('Description', validators=[DataRequired()])


class HumanListForm(FlaskForm):
    humans = SelectField('Humans', coerce=int)


class MovieListForm(FlaskForm):
    movies = SelectField('Movies', coerce=int)
