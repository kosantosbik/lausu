from datetime import datetime
from flaskblog import db, login_manager
from flask_login import UserMixin
from sqlalchemy import UniqueConstraint


@login_manager.user_loader
def load_user(user_id):
    return Users.query.get(int(user_id))


class Groups(db.Model):
    # 0 for normal users 1 for administrators
    id = db.Column(db.Integer, primary_key=True)
    users = db.relationship('Users', backref='group', lazy=True)

    def __repr__(self):
        if self.id == 1:
            return "Groups('User')"
        else:
            return "Groups('Administrator')"


class Users(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    group_id = db.Column(db.Integer, db.ForeignKey('groups.id'), nullable=False)
    username = db.Column(db.String(20), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(60), nullable=False)
    human_lists = db.relationship('HumanLists', backref='author', lazy=True)
    movie_lists = db.relationship('MovieLists', backref='author', lazy=True)

    def __repr__(self):
        return f"Users('{self.id}', '{self.group_id}', '{self.username}', '{self.email}')"


class Humans(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nconst = db.Column(db.String(10), unique=True, nullable=False)
    name = db.Column(db.String(60), nullable=False)
    birth_year = db.Column(db.Integer)
    death_year = db.Column(db.Integer)
    picture = db.Column(db.String(200))

    def __repr__(self):
        return f"Humans('{self.id}','{self.nconst}','{self.name}')"


class Movies(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    tconst = db.Column(db.String(10), unique=True, nullable=False)
    type = db.Column(db.String(60), nullable=False)
    title = db.Column(db.String(80), nullable=False)
    start_year = db.Column(db.Integer, nullable=False)
    end_year = db.Column(db.Integer)
    runtime = db.Column(db.Integer)
    picture = db.Column(db.String(200))

    def __repr__(self):
        return f"Movies('{self.id}', '{self.tconst}', '{self.type}', '{self.title}', '{self.start_year}')"


class MovieLists(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    name = db.Column(db.String(50), nullable=False)
    description = db.Column(db.String(250), nullable=False)

    def __repr__(self):
        return f"MovieLists('{self.id}', '{self.user_id}', '{self.name}')"


class HumanLists(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    name = db.Column(db.String(50), nullable=False)
    description = db.Column(db.String(250), nullable=False)

    def __repr__(self):
        return f"HumanLists('{self.id}', '{self.user_id}', '{self.name}')"


class MovieListMovies(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    movie_list_id = db.Column(db.Integer, db.ForeignKey('movie_lists.id'), nullable=False)
    movie_id = db.Column(db.Integer, db.ForeignKey('movies.id'), nullable=False)
    __table_args__ = (UniqueConstraint('movie_list_id', 'movie_id'),
                     )

    def __repr__(self):
        return f"MovieListMovies('{self.id}', '{self.movie_list_id}', '{self.movie_id}')"


class HumanListHumans(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    human_list_id = db.Column(db.Integer, db.ForeignKey('human_lists.id'), nullable=False)
    human_id = db.Column(db.Integer, db.ForeignKey('humans.id'), nullable=False)
    __table_args__ = (UniqueConstraint('human_list_id', 'human_id'),
                     )

    def __repr__(self):
        return f"HumanListHumans('{self.id}', '{self.movie_list_id}', '{self.movie_id}')"
