from flask import render_template, url_for, flash, redirect, request
from flaskblog import app, db, bcrypt
from flaskblog.forms import *
from flaskblog.models import *
from flask_login import login_user, current_user, logout_user, login_required
from flask import abort

posts = []


@app.route("/")
@app.route("/home")
def home():
    return render_template('home.html', posts=posts)


@app.route("/about")
def about():
    return render_template('about.html', title='About')

@app.route("/admin")
@login_required
def admin():
    if current_user.group_id == 1:
        return render_template('admin.html', title='Admin')
    else:
        flash('You are not authorized for this area!', 'danger')
        return redirect(url_for('home'))

@app.route("/admin/movie", methods=['GET', 'POST'])
@login_required
def admin_movie():
    if current_user.group_id == 1:
        form = AdminMovieForm()
        if request.method == 'POST' and form.validate():
            movie = Movies(tconst=form.tconst.data,
                           type=form.type.data,
                           title=form.title.data,
                           start_year=form.start_year.data,
                           end_year=form.end_year.data,
                           runtime=form.runtime.data,
                           picture=form.picture.data)
            db.session.add(movie)
            db.session.commit()
            flash('Movie has been added to database.', 'success')
            return redirect(url_for('admin_movie'))
        else:
            return render_template('admin_movie.html', title='MovieAdmin', form=form)
    else:
        flash('You are not authorized for this area!', 'danger')
        return redirect(url_for('home'))

@app.route("/admin/human", methods=['GET', 'POST'])
@login_required
def admin_human():
    if current_user.group_id == 1:
        form = AdminHumanForm()
        if request.method == 'POST' and form.validate():
            human = Humans(nconst=form.nconst.data,
                           name=form.name.data,
                           birth_year=form.birth_year.data,
                           death_year=form.death_year.data,
                           picture=form.picture.data)
            db.session.add(human)
            db.session.commit()
            flash('Human has been added to database.', 'success')
            return redirect(url_for('admin_human'))
        else:
            return render_template('admin_human.html', title='HumanAdmin', form=form)
    else:
        flash('You are not authorized for this area!', 'danger')
        return redirect(url_for('home'))

@app.route("/register", methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = RegistrationForm()
    if request.method == 'POST' and form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = Users(username=form.username.data,
                     email=form.email.data,
                     password=hashed_password,
                     group_id=0)
        db.session.add(user)
        db.session.commit()
        flash('Your account has been created! You are now able to log in', 'success')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route("/login", methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = LoginForm()
    if request.method == 'POST' and form.validate_on_submit():
        user = Users.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            return redirect(url_for('home'))
        else:
            flash('Login Unsuccessful. Please check email and password', 'danger')
    return render_template('login.html', title='Login', form=form)


@app.route("/movielist/create", methods=['GET', 'POST'])
@login_required
def movielist_create():
    form = CreateMovieListForm()
    if request.method == 'POST' and form.validate():
            movielist = MovieLists(user_id=current_user.id,
                                   name=form.name.data,
                                   description=form.description.data)
            db.session.add(movielist)
            db.session.commit()
            flash('New movie list has been created. You can add movies to your list now', 'success')
            return redirect(url_for('movielist_create'))

    return render_template('movielist_create.html',
                           title='Create Movie List',
                           form=form)


@app.route("/humanlist/create", methods=['GET', 'POST'])
@login_required
def humanlist_create():
    form = CreateHumanListForm()
    if request.method == 'POST' and form.validate():
            humanlist = HumanLists(user_id=current_user.id,
                                   name=form.name.data,
                                   description=form.description.data)
            db.session.add(humanlist)
            db.session.commit()
            flash('New human list has been created. You can add humans to your list now', 'success')
            return redirect(url_for('humanlist_create'))

    return render_template('humanlist_create.html',
                           title='Create Human List',
                           form=form)


@app.route("/humanlist/id/<int:list_id>", methods=['GET', 'POST'])
@login_required
def humanlist(list_id):
    humans = Humans.query.all()
    form = HumanListForm(obj=humans)
    form.humans.choices = [(h.id, h.name) for h in humans]
    humans_in_list = Humans.query.join(HumanListHumans).filter(HumanListHumans.human_id == Humans.id).filter(HumanListHumans.human_list_id == list_id).all()
    if request.method == 'POST' and form.validate():
        hl_human = HumanListHumans(human_list_id=list_id,
                                   human_id=form.data['humans'])
        db.session.add(hl_human)
        db.session.commit()
        flash('New human list human has been added', 'success')
        return redirect(url_for('humanlist', list_id=list_id))

    return render_template('humanlist.html',
                           title='Humanlist',
                           form=form,
                           humans=humans_in_list)


@app.route("/movielist/id/<int:list_id>", methods=['GET', 'POST'])
@login_required
def movielist(list_id):
    movies = Movies.query.all()
    form = MovieListForm(obj=movies)
    form.movies.choices = [(h.id, h.title) for h in movies]
    movies_in_list = Movies.query.join(MovieListMovies).filter(MovieListMovies.movie_id == Movies.id).filter(MovieListMovies.movie_list_id == list_id).all()
    if request.method == 'POST' and form.validate():
        ml_movie = MovieListMovies(movie_list_id=list_id,
                                   movie_id=form.data['movies'])
        db.session.add(ml_movie)
        db.session.commit()
        flash('New movie list movie has been added', 'success')
        return redirect(url_for('humanlist', list_id=list_id))

    return render_template('movielist.html',
                           title='Movielist',
                           form=form,
                           movies=movies_in_list)


@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('home'))


@app.route("/account")
@login_required
def account():
    return render_template('account.html', title='Account')
